#!/bin/bash
sudo yum install rpmdevtools rpmlint tree -y
cd ~/
rpmdev-setuptree
git clone git@gitlab.training.net:emagradze/testproject.git
mkdir hello-0.0.1
mv ~/testproject/hello.sh ~/hello-0.0.1/
mv ~/testproject/hello.spec ~/rpmbuild/SPECS/
cd ~/
tar -zcvf hello-0.0.1.tar.gz hello-0.0.1
mv hello-0.0.1.tar.gz rpmbuild/SOURCES
cd rpmbuild/SPECS
#rpmdev-newspec hello
rpmlint hello.spec
cd ~/
rpmbuild -bs ~/rpmbuild/SPECS/hello.spec
rpmbuild -bb ~/rpmbuild/SPECS/hello.spec
tree ~/rpmbuild/
ls ~/rpmbuild/RPMS/noarch/
rm -rf ~/testproject
